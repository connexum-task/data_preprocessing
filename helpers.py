def check_duplicates(data, key='title'):
    titles = set()
    duplicates = []
    for elem in data:
        if elem[key] in titles:
            duplicates.append(elem[key])
            print("Duplicate found:", elem[key])
        titles.add(elem[key])
    print('{} duplicates found.\n'.format(len(duplicates)))


def check_not_nulls(data):
    for elem in data:
        if not elem['title'] or not elem['titleEn']:
            print('There are Nulls')
