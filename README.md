# Solution

## Requirements

* For running Python 3.6, I recommend having a virtualenv.
* For the MongoDB, I am using docker-compose. If you wish, you could 
have your own MongoDB installed locally. Just be sure you have set these env variables: DB_HOST, DB_PORT.
Default are: `localhost` and `27017` respectively.

## How to run
In TASK1 directory you will find the data pre-processing script with python. 

1. First install the dependencies in a virtualenv by:
```bash
pip install -r requirements.txt
```

2. Get the MongoDB Docker instance up and running.
```bash
docker-compose up
```

3. Open a new terminal and run the script
```bash
python data_cleaner.py news_list.json
```

4. Go to TASK2 directory and install dependencies
```bash
npm install 
```

5. Run the server
```bash
npm start
```

6. Test the API

## API endpoints

### Endpoint 1: Grouped by nationality
GET `/news/by_nationality`

```bash
http GET "http://localhost:3000/news/by_nationality"
```

### Endoint 2: Within 100km of given location
GET `/news/closest`
```bash
http GET "http://localhost:3000/news/closest?lat=44.54&lon=7.54&language=it"
```

Query Params:
* lat 
* lon
* language: Example 'it'

### Endpoint 3: Match news by search

I chose the keyword approach.

POST `/news/search`
```bash
http POST "http://localhost:3000/news/search?querySearch=Russia%20Italia%20DAYS%20ago"
```

### Endpoint 4: Get news after a given date
GET `/news`

```bash
http GET "http://localhost:3000/news?nDays=50"
```
