import json
import sys
import datetime
import re
import string
import os
from pymongo import MongoClient
from helpers import check_duplicates, check_not_nulls

DB_HOST = os.environ.get('DB_HOST', 'localhost')
DB_PORT = os.environ.get('DB_PORT', 27017)


def remove_duplicates(data):
    cleaned_data = []
    titles_set = set()
    for new in data:
        if new['title'] and new['titleEn']:
            if not new['title'] in titles_set:
                titles_set.add(new['title'])
                cleaned_data.append(new)
    return cleaned_data


def make_keywords(title):
    title = title.lower()
    title = re.sub(r'\d+', '', title)
    title = title.translate(str.maketrans('', '', string.punctuation))
    title = title.strip()
    tokens = title.split()
    return tokens


if __name__ == "__main__":
    if len(sys.argv) < 2:
        exit(1)

    try:
        with open(sys.argv[1]) as json_file:
            data = json.load(json_file)
    except(FileNotFoundError):
        print("File Not Found")
        exit(1)

    check_duplicates(data, 'title')
    cleaned_news = remove_duplicates(data)
    check_duplicates(cleaned_news)
    check_not_nulls(cleaned_news)

    for elem in cleaned_news:
        elem['keywords'] = make_keywords(elem['titleEn'])
        elem['title_en'] = elem.pop('titleEn')
        elem['publication_date'] = datetime.datetime.strptime(
            elem.pop('pubDate'), "%Y-%m-%dT%H:%M:%S.%fZ"
        )
        
        if not elem['cities']:
            elem['cities'] = []

        for city in elem['cities']:
            coordinates = city['CityCoord']['coordinates']
            location = {}
            location['type'] = "Point"
            location['coordinates'] = coordinates
            city['location'] = location
            del city['CityCoord']

    client = MongoClient(DB_HOST, DB_PORT)
    news = client.connexum.news
    news.drop()

    for i in cleaned_news:
        news.insert_one(i)

    print("done!")